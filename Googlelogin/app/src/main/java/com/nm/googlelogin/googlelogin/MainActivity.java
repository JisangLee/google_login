package com.nm.googlelogin.googlelogin;

import android.content.IntentSender;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
/*
 * 160811 / Author : Michael
 * 수정내용 : 로그인 버튼 로그아웃으로 바뀌고 로그아웃 활성화시키기
 */
public class MainActivity extends AppCompatActivity implements  GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "MainActivity";
    private GoogleApiClient mGoogleApiClient;

    private TextView userName,userId;
    private ImageView userPhoto;
    private Button btn_con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userName = (TextView) findViewById(R.id.userName);
        userId = (TextView)findViewById(R.id.userId);
        userPhoto = (ImageView)findViewById(R.id.userPhoto);
        btn_con = (Button)findViewById(R.id.btn_con);

    }
    public void mOnClick(View view){
        switch (view.getId()){
            case R.id.btn_con:
                if(mGoogleApiClient == null){
                    Toast.makeText(this, "접속합니다", Toast.LENGTH_SHORT).show();

                    mGoogleApiClient = new GoogleApiClient.Builder(this)
                            .addConnectionCallbacks(this)
                            .addOnConnectionFailedListener(this)
                            .addApi(Plus.API)
                            .addScope(Plus.SCOPE_PLUS_PROFILE)
                            .build();

                    mGoogleApiClient.connect();

                }else{
                    mGoogleApiClient.disconnect();
                    mGoogleApiClient=null;
                    userId.setText("로그인을 하세요.(아이디text)");
                    userName.setText("로그인을 하세요.(이름text)");
                    userPhoto.setImageResource(R.drawable.ic_launcher);
                    btn_con.setText("로그인");

                }


                break;

        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "구글 플레이 연결이 되었습니다.");

        if (!mGoogleApiClient.isConnected() || Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) == null) {

            Log.d(TAG, "onConnected 연결 실패");

        } else {
            Log.d(TAG, "onConnected 연결 성공");

            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);

            if (currentPerson.hasImage()) {

                Log.d(TAG, "이미지 경로는 : " + currentPerson.getImage().getUrl());
                //사용자 이미지 가져와서 이미지뷰에 넣기.
               Glide.with(MainActivity.this)
                        .load(currentPerson.getImage().getUrl())
                        .into(userPhoto);
                //버튼 로그아웃으로 바꾸기
                btn_con.setText("로그아웃");


            }
            if (currentPerson.hasDisplayName()) {
                Log.d(TAG,"디스플레이 이름 : "+ currentPerson.getDisplayName());
                Log.d(TAG, "디스플레이 아이디는 : " + currentPerson.getId());
                userId.setText(currentPerson.getId());
                userName.setText(currentPerson.getDisplayName());
            }

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "연결 에러 " + connectionResult);

        if (connectionResult.hasResolution()) {

            Log.e(TAG,
                    String.format(
                            "Connection to Play Services Failed, error: %d, reason: %s",
                            connectionResult.getErrorCode(),
                            connectionResult.toString()));
            try {
                //이게 핵심?
                connectionResult.startResolutionForResult(this, 0);
            } catch (IntentSender.SendIntentException e) {
                Log.e(TAG, e.toString(), e);
            }
        }else{
            Toast.makeText(getApplicationContext(), "이미 로그인 중", Toast.LENGTH_SHORT).show();
        }
    }
}
